<?php

declare(strict_types = 1);

namespace App\Service;

use App\Entity\Presence;
use App\Entity\Room;
use App\Entity\Student;
use Doctrine\ORM\EntityManagerInterface;

class PresenceService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create(Student $student, Room $room, int $timestamp): void
    {
        $presence = new Presence();

        $datetime = new \DateTime();
        $datetime->setTimestamp($timestamp);

        $presence->setStudent($student);
        $presence->setRoom($room);
        $presence->setDatetime($datetime);

        $this->em->persist($presence);
        $this->em->flush();
    }
}
