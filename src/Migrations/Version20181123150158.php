<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181123150158 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecture DROP INDEX UNIQ_C167794854177093, ADD INDEX IDX_C167794854177093 (room_id)');
        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C167794854177093');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C167794854177093 FOREIGN KEY (room_id) REFERENCES room (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecture DROP INDEX IDX_C167794854177093, ADD UNIQUE INDEX UNIQ_C167794854177093 (room_id)');
        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C167794854177093');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C167794854177093 FOREIGN KEY (room_id) REFERENCES lecture (id)');
    }
}
