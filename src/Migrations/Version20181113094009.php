<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181113094009 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lecture (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', room_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_C167794854177093 (room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', code VARCHAR(5) NOT NULL, note VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', chip_id INT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE students_lectures (student_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', lecture_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_7F07066ECB944F1A (student_id), INDEX IDX_7F07066E35E32FCD (lecture_id), PRIMARY KEY(student_id, lecture_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schedule (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', lecture_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', start DATETIME NOT NULL, end DATETIME NOT NULL, INDEX IDX_5A3811FB35E32FCD (lecture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C167794854177093 FOREIGN KEY (room_id) REFERENCES lecture (id)');
        $this->addSql('ALTER TABLE students_lectures ADD CONSTRAINT FK_7F07066ECB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE students_lectures ADD CONSTRAINT FK_7F07066E35E32FCD FOREIGN KEY (lecture_id) REFERENCES lecture (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE schedule ADD CONSTRAINT FK_5A3811FB35E32FCD FOREIGN KEY (lecture_id) REFERENCES lecture (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C167794854177093');
        $this->addSql('ALTER TABLE students_lectures DROP FOREIGN KEY FK_7F07066E35E32FCD');
        $this->addSql('ALTER TABLE schedule DROP FOREIGN KEY FK_5A3811FB35E32FCD');
        $this->addSql('ALTER TABLE students_lectures DROP FOREIGN KEY FK_7F07066ECB944F1A');
        $this->addSql('DROP TABLE lecture');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE students_lectures');
        $this->addSql('DROP TABLE schedule');
    }
}
