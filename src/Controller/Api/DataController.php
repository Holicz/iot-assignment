<?php

declare(strict_types = 1);

namespace App\Controller\Api;

use App\Entity\Room;
use App\Entity\Student;
use App\Service\PresenceService;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api")
 */
class DataController extends AbstractController
{
    private $presenceService;

    public function __construct(PresenceService $presenceService)
    {
        $this->presenceService = $presenceService;
    }

    /**
     * @Route("/data", name="api_data_receive", methods={"POST"})
     */
    public function receive(Request $request): Response
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        if (($handle = fopen($file->getRealPath(), 'rb')) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                $studentRepository = $this->getDoctrine()->getRepository(Student::class);
                $student = $studentRepository->findOneBy(['chipId' => $data[2]]);

                $roomRepository = $this->getDoctrine()->getRepository(Room::class);
                $room = $roomRepository->findOneBy(['code' => $data[1]]);

                if (!$student || !$room) {
                    continue;
                }

                $this->presenceService->create($student, $room, (int) $data[0]);
            }

            fclose($handle);
        }

        return new Response();
    }
}
