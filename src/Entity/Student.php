<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Student
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lecture", inversedBy="students", cascade={"persist"})
     * @ORM\JoinTable(name="students_lectures")
     */
    private $lectures;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $chipId;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $schoolId;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $firstname;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $lastname;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Presence", mappedBy="student")
     */
    private $presences;

    public function __construct()
    {
        $this->lectures = new ArrayCollection();
        $this->presences = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName() . ' (' . $this->getSchoolId() . ')';
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function addLecture(Lecture $lecture): void
    {
        $this->lectures->add($lecture);
    }

    public function removeLecture(Lecture $lecture): void
    {
        $this->lectures->removeElement($lecture);
    }

    public function setChipId(string $chipId): void
    {
        $this->chipId = $chipId;
    }

    public function getChipId(): ?string
    {
        return $this->chipId;
    }

    public function setSchoolId(string $schoolId): void
    {
        $this->schoolId = $schoolId;
    }

    public function getSchoolId(): ?string
    {
        return $this->schoolId;
    }

    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function getName(): string
    {
        return $this->getLastname() . ' ' . $this->getFirstname();
    }

    public function addPresence(Presence $presence): void
    {
        $this->presences->add($presence);
    }

    public function getPresences(): Collection
    {
        return $this->presences;
    }
}
