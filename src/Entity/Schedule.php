<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Schedule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lecture", inversedBy="schedules")
     * @ORM\JoinColumn(name="lecture_id", referencedColumnName="id")
     *
     * @var Lecture
     */
    private $lecture;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $start;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $end;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setLecture(Lecture $lecture): void
    {
        $this->lecture = $lecture;
    }

    public function getLecture(): ?Lecture
    {
        return $this->lecture;
    }

    public function setStart(\DateTime $start): void
    {
        $this->start = $start;
    }

    public function getStart(): ?\DateTime
    {
        return $this->start;
    }

    public function setEnd(\DateTime $end): void
    {
        $this->end = $end;
    }

    public function getEnd(): ?\DateTime
    {
        return $this->end;
    }
}