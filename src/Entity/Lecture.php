<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Lecture
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     *
     * @var Room
     */
    private $room;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Student", mappedBy="lectures", cascade={"persist"})
     *
     * @var Student[]
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Schedule", mappedBy="lecture")
     *
     * @var Schedule[]
     */
    private $schedules;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->schedules = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setRoom(Room $room): void
    {
        $this->room = $room;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function addStudent(Student $student): void
    {
        $this->students->add($student);
        $student->addLecture($this);
    }

    public function removeStudent(Student $student): void
    {
        $student->removeLecture($this);
        $this->students->removeElement($student);
    }

    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function getSchedules(): Collection
    {
        return $this->schedules;
    }
}
