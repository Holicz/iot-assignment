<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Presence
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Student", inversedBy="presences")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id")
     *
     * @var Student
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     *
     * @var Room
     */
    private $room;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $datetime;

    public function __toString(): string
    {
        $date = date_timezone_set($this->getDatetime(), timezone_open('Europe/Copenhagen'));
        $date = date_format($date, 'd.m.Y H:i');
        return 'Room ' . $this->getRoom()->getCode() . ' at ' . $date;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setStudent(Student $student): void
    {
        $this->student = $student;
    }

    public function getStudent(): Student
    {
        return $this->student;
    }

    public function setRoom(Room $room): void
    {
        $this->room = $room;
    }

    public function getRoom(): Room
    {
        return $this->room;
    }

    public function setDatetime(\DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }
}
