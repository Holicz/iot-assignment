Installation
========================

1. Clone the repo and go inside the project folder
```bash
git clone https://gitlab.com/Holicz/iot-assignment.git iot-assignment
cd iot-assignment
```
2. Install vendor ([how to install composer](https://getcomposer.org/doc/00-intro.md#globally))
```bash
composer install
```
3. Configure your connection to the database in file `.env`
4. Run migrations
```bash
php bin/console doctrine:migrations:migrate
```
5. Run the server
```bash
php bin/console server:run
```
6. Go to `http://127.0.0.1:8000`
7. Profit